/************
** HEADERS **
************/
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <vector>

#include <cuda_runtime.h>
#include <cusparse.h>

#include <thrust/copy.h>
#include <thrust/device_vector.h>
#include <thrust/equal.h>
#include <thrust/host_vector.h>
#include <thrust/unique.h>


#define CHECK_CUDA(func)                                                       \
{                                                                              \
    cudaError_t status = (func);                                               \
    if (status != cudaSuccess) {                                               \
        printf("CUDA API failed at line %d with error: %s (%d)\n",             \
               __LINE__, cudaGetErrorString(status), status);                  \
        return EXIT_FAILURE;                                                   \
    }                                                                          \
}

#define CHECK_CUSPARSE(func)                                                   \
{                                                                              \
    cusparseStatus_t status = (func);                                          \
    if (status != CUSPARSE_STATUS_SUCCESS) {                                   \
        printf("CUSPARSE API failed at line %d with error: %s (%d)\n",         \
               __LINE__, cusparseGetErrorString(status), status);              \
        return EXIT_FAILURE;                                                   \
    }                                                                          \
}

/**********************
** GLOBAL PARAMETERS **
**********************/

const int INPUT_SIZE = 60000;

int NUM_TRIALS = 1;
int BATCH_SIZE = 600;
int NUM_BATCH  = 100;

std::vector<int> NEURONS  {1024, 4096, 16384, 65536};
std::vector<int> LAYERS   {120, 480, 1920};
float            BIAS[] = {-0.3,-0.35,-0.4,-0.45};

std::string INPUT_FILENAME = "./data/sparse-images-";
std::string NETWK_FILENAME = "./arch/neuron";
std::string LABEL_FILENAME = "./label/neuron";


/***********************
** GLOBAL SHARED DATA **
***********************/
// Input image dataset (60000*NEURON)
int *    imageDataRowHost = 0;
int *    imageDataColHost = 0;
double * imageDataValHost = 0;

// Output labels (60000*1)
int *   labelDataHost = 0;

thrust::host_vector<int>   labelOutHost;
thrust::device_vector<int> labelOutDev;

// Weight Matrix (NEURON*NEURON*LAYER)
int **    weightDataRowHost = 0;
int **    weightDataColHost = 0;
double ** weightDataValHost = 0;
int *     weightDataNNZHost = 0;

int **    weightDataRowDev = 0;
int **    weightDataColDev = 0;
double ** weightDataValDev = 0;

// Bias Vector (NEURON*1)
double * biasVectorHost = 0;
double * biasVectorDev  = 0;

// CuSPARSE Library handle
cusparseHandle_t cuspHandle = 0;
cusparseMatDescr_t genericDescr = 0;

        
/*****************
** CUDA KERNELS **
*****************/

// Kernel to add bias and apply activation
//  Updates y as sigma(y + b) in place. 
//  y: BATCH_SIZE x WIDTH
//  b: WIDTH x 1
//  sigma: Capped ReLU

__global__ void biasActivation(double * y, const double * bias, const int rows, const int cols) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    for ( ; idx < rows*cols; idx+=stride ) {
        y[idx] += bias[ (idx - (idx % rows))/rows ];
        if ( y[idx] < 0  ) y[idx] = 0.0;
        else if ( y[idx] > 32 ) y[idx] = 32.0;
    }
}

__global__ void subOffset(int * y, const int offset, const int size) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    for ( ; idx < size; idx+=stride ) y[idx] -= offset;
}

/*********************
** HELPER FUNCTIONS **
*********************/

// Image data in file is provided in COO format.
int loadImageData(int width) {
    std::ifstream file;
    int size = 0;

    file.open(INPUT_FILENAME + std::to_string(width) + "_row.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    imageDataRowHost = (int *) malloc(size);
    file.read( (char *) imageDataRowHost, size );
    file.close();

    size = 0;
    file.open(INPUT_FILENAME + std::to_string(width) + "_col.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    imageDataColHost = (int *) malloc(size);
    file.read( (char *) imageDataColHost, size );
    file.close();

    size = 0;
    file.open(INPUT_FILENAME + std::to_string(width) + "_val.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    imageDataValHost = (double *) malloc(size);
    file.read( (char *) imageDataValHost, size );
    file.close();

    return size/sizeof(imageDataValHost[0]);
}

// Labels is a sparse vector of images that are classified in atleast one category
int loadLabels(int width, int depth) {
    std::ifstream file;
    int size = 0;

    file.open(LABEL_FILENAME + std::to_string(width) + "-l" + std::to_string(depth) + "-categories.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    labelDataHost = (int *) malloc(size);
    file.read( (char *) labelDataHost, size);
    file.close();

    return size/sizeof(labelDataHost[0]);
}

// Weights for a given layer are provided in COO format.
// This function converts them to CSR before storing.
int loadWeight(int width, int layer) {
    std::ifstream file;
    int size = 0;

    int *    weightRowTmp = 0;
    int *    weightColTmp = 0;
    double * weightValTmp = 0;

    file.open(NETWK_FILENAME + std::to_string(width) + "/n" + std::to_string(width) + "-l" + std::to_string(layer) + "_row.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    weightRowTmp = (int *)    malloc(size);
    file.read( (char *) weightRowTmp, size ); 
    file.close();
    
    size = 0;
    file.open(NETWK_FILENAME + std::to_string(width) + "/n" + std::to_string(width) + "-l" + std::to_string(layer) + "_col.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    weightColTmp = (int *)    malloc(size);
    file.read( (char *) weightColTmp, size ); 
    file.close();
    int nnz = size/sizeof(weightColTmp[0]);

    size = 0;
    file.open(NETWK_FILENAME + std::to_string(width) + "/n" + std::to_string(width) + "-l" + std::to_string(layer) + "_val.bin", std::ios::binary | std::ios::in);
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);
    weightValTmp = (double *)    malloc(size);
    file.read( (char *) weightValTmp, size ); 
    file.close();

    CHECK_CUDA( cudaMalloc( (void**) &weightDataRowHost[layer], (width+1)*sizeof(weightDataRowHost[layer][0]) ) )
    CHECK_CUDA( cudaMalloc( (void**) &weightDataColHost[layer], nnz      *sizeof(weightDataColHost[layer][0]) ) )
    CHECK_CUDA( cudaMalloc( (void**) &weightDataValHost[layer], nnz      *sizeof(weightDataValHost[layer][0]) ) )

    CHECK_CUDA( cudaMemcpy( weightDataRowHost[layer], weightRowTmp, (size_t) (width+1)*sizeof(weightDataRowHost[layer][0]), cudaMemcpyHostToDevice ) )
    CHECK_CUDA( cudaMemcpy( weightDataColHost[layer], weightColTmp, (size_t) nnz      *sizeof(weightDataColHost[layer][0]), cudaMemcpyHostToDevice ) )
    CHECK_CUDA( cudaMemcpy( weightDataValHost[layer], weightValTmp, (size_t) nnz      *sizeof(weightDataValHost[layer][0]), cudaMemcpyHostToDevice ) )

    free(weightRowTmp);
    free(weightColTmp);
    free(weightValTmp); 

    return nnz;
}

int initGPU(int width, int depth, int imgDataSize, int * weightSize) {
    // Allocate Device Memory
    CHECK_CUDA( cudaMalloc( (void**) &biasVectorDev, width*sizeof(biasVectorDev[0]) ) )

    CHECK_CUDA( cudaMalloc( (void**) &weightDataRowDev, depth*sizeof(weightDataRowDev[0]) ) ) 
    CHECK_CUDA( cudaMalloc( (void**) &weightDataColDev, depth*sizeof(weightDataColDev[0]) ) )
    CHECK_CUDA( cudaMalloc( (void**) &weightDataValDev, depth*sizeof(weightDataValDev[0]) ) )

    // Copy data from host to device
    
    CHECK_CUDA( cudaMemcpy( weightDataRowDev, weightDataRowHost, (size_t) depth*sizeof(weightDataRowDev[0]), cudaMemcpyHostToDevice ) )
    CHECK_CUDA( cudaMemcpy( weightDataColDev, weightDataColHost, (size_t) depth*sizeof(weightDataColDev[0]), cudaMemcpyHostToDevice ) )
    CHECK_CUDA( cudaMemcpy( weightDataValDev, weightDataValHost, (size_t) depth*sizeof(weightDataValDev[0]), cudaMemcpyHostToDevice ) ) 

    // Initialise Bias vector
    CHECK_CUDA( cudaMemcpy( biasVectorDev, biasVectorHost, (size_t) width*sizeof(biasVectorDev[0]), cudaMemcpyHostToDevice ) )
    

    // Initialise CuSPARSE Library
    CHECK_CUSPARSE( cusparseCreate(&cuspHandle) )

    // Initialise matrix descriptors
    CHECK_CUSPARSE( cusparseCreateMatDescr(&genericDescr) )

    // Initialise GPU
    int dev_id;
    cudaDeviceProp prop;

    CHECK_CUDA( cudaGetDevice(&dev_id) )
    CHECK_CUDA( cudaGetDeviceProperties(&prop, dev_id) )

    return EXIT_SUCCESS;
}

void freeGPU(int depth) {
    // Free Device memory
    cudaFree(biasVectorDev);

    cudaFree(weightDataRowDev);
    cudaFree(weightDataColDev);
    cudaFree(weightDataValDev);

    labelOutDev.clear();
    labelOutDev.shrink_to_fit();

    // Destroy matrix descriptors
    cusparseDestroyMatDescr(genericDescr);
    
    // Destroy library handle
    cusparseDestroy(cuspHandle);

    // Reset device
    cudaDeviceReset();
}

int runInference(int width, int depth, int * weightSize) {
    // Initilise variables
    csrgemm2Info_t info = NULL;
    
    int nnzC = 0;
    size_t bufferSize = 0;
    void *buffer = NULL;
    int *nnzTotalDevHostPtr = &nnzC;
    double alpha = 1.0;

    cusparseMatDescr_t descrY = genericDescr;
    cusparseMatDescr_t descrC = genericDescr;
    cusparseMatDescr_t descrW = genericDescr;

    int *    cRowDev = 0;
    int *    cColDev = 0;
    double * cValDev = 0;
    double * cDense  = 0;

    int * nnzPerRow = 0; 

    int numThreads = 1024;  
    int numBlocks = ceil(1.0*BATCH_SIZE*width/numThreads);


    // Initialise library something
    CHECK_CUSPARSE( cusparseCreateCsrgemm2Info(&info) )
    //cudaDeviceSynchronize();

    for ( int batch = 0; batch < NUM_BATCH; batch++ ) {
        int *    yRowDev = 0;
        int *    yColDev = 0;
        double * yValDev = 0;

        int imgDataSize = imageDataRowHost[BATCH_SIZE*(batch+1)] - imageDataRowHost[BATCH_SIZE*batch];
        int offset = imageDataRowHost[BATCH_SIZE*batch];

        CHECK_CUDA( cudaMalloc( (void**) &yRowDev, (BATCH_SIZE+1)*sizeof(yRowDev[0]) ) )
        CHECK_CUDA( cudaMalloc( (void**) &yColDev, imgDataSize   *sizeof(yColDev[0]) ) )
        CHECK_CUDA( cudaMalloc( (void**) &yValDev, imgDataSize   *sizeof(yValDev[0]) ) )
        
        
        CHECK_CUDA( cudaMemcpy( yRowDev, imageDataRowHost + batch*BATCH_SIZE, (size_t) (BATCH_SIZE+1)*sizeof(yRowDev[0]), cudaMemcpyHostToDevice ) ) 
        CHECK_CUDA( cudaMemcpy( yColDev, imageDataColHost + offset, (size_t) imgDataSize   *sizeof(yColDev[0]), cudaMemcpyHostToDevice ) )
        CHECK_CUDA( cudaMemcpy( yValDev, imageDataValHost + offset, (size_t) imgDataSize   *sizeof(yValDev[0]), cudaMemcpyHostToDevice ) )

        subOffset<<<numBlocks,numThreads>>>( yRowDev, offset, BATCH_SIZE + 1 );
        cudaDeviceSynchronize();
        
        int nnzY = imgDataSize;
        int * nnzYPtr = &nnzY;
        
        //std::cout<<batch<<std::endl;

        for ( int i = 0; i < depth; i++ ) {
            //std::cout<<"\t"<<i<<std::endl;
            /****    Compute C = Y*W    ****/
            // Initialise variables
            buffer  = NULL;        
            cRowDev = 0;
            cColDev = 0;
            cValDev = 0;
            cDense  = 0;
            nnzPerRow = 0;

            //std::cout<<"\tnnzY: "<<nnzY<<"; nnzW: "<<weightSize[i]<<std::endl;

            // Allocate buffer
            CHECK_CUSPARSE( cusparseDcsrgemm2_bufferSizeExt( cuspHandle, BATCH_SIZE, width, width, \
                                                                &alpha, descrY, nnzY, yRowDev, yColDev, \
                                                                descrW, weightSize[i], weightDataRowHost[i], weightDataColHost[i], \
                                                                NULL, genericDescr, 0, NULL, NULL, info, &bufferSize ) )
            //cudaDeviceSynchronize();
            CHECK_CUDA( cudaMalloc( &buffer, bufferSize ) )
            //cudaDeviceSynchronize();

            // Allocate C
            CHECK_CUDA( cudaMalloc( (void**) &cRowDev, (BATCH_SIZE+1)*sizeof(cRowDev[0]) ) )
            //cudaDeviceSynchronize();

            // Compute NNZ
            //std::cout<<"yRowDev: "<<yRowDev<<"\t"<<yRowDev + BATCH_SIZE + 1<<std::endl;
            //std::cout<<"yColDev: "<<yColDev<<"\t"<<yColDev + imgDataSize<<std::endl;
            //std::cout<<"weightDataRowHost[i]: "<<weightDataRowHost[i]<<"\t"<<weightDataRowHost[i]+width<<std::endl;
            //std::cout<<"weightDataColHost[i]: "<<weightDataColHost[i]<<"\t"<<weightDataColHost[i]+weightSize[i]<<std::endl;
            //std::cout<<"cRowDev: "<<cRowDev<<"\t"<<cRowDev+ BATCH_SIZE+1<<std::endl;
            //std::cout<<"Buffer: "<<buffer<<"\t"<<buffer+bufferSize<<std::endl;
            CHECK_CUSPARSE( cusparseXcsrgemm2Nnz( cuspHandle, BATCH_SIZE, width, width, \
                                                    descrY, nnzY, yRowDev, yColDev, \
                                                    descrW, weightSize[i], weightDataRowHost[i], weightDataColHost[i], \
                                                    genericDescr, 0, NULL, NULL, \
                                                    descrC, cRowDev, nnzTotalDevHostPtr, \
                                                    info, buffer ) )
            cudaDeviceSynchronize();

            //std::cout<<"\tnnzC: "<<nnzC<<std::endl;

            // Compute C
            CHECK_CUDA( cudaMalloc( (void**) &cColDev, nnzC*sizeof(cColDev[0]) ) )
            CHECK_CUDA( cudaMalloc( (void**) &cValDev, nnzC*sizeof(cValDev[0]) ) )
            //cudaDeviceSynchronize();

            CHECK_CUSPARSE( cusparseDcsrgemm2( cuspHandle, BATCH_SIZE, width, width, \
                                                &alpha, descrY, nnzY, yValDev, yRowDev, yColDev, \
                                                descrW, weightSize[i], weightDataValHost[i], weightDataRowHost[i], weightDataColHost[i], \
                                                NULL, genericDescr, 0, NULL, NULL, NULL, \
                                                descrC, cValDev, cRowDev, cColDev, \
                                                info, buffer ) )
            cudaDeviceSynchronize();


            /****   Add bias and activation     ****/

            // Convert C to a dense matrix
            CHECK_CUDA( cudaMalloc( (void**) &cDense, BATCH_SIZE*width*sizeof(cDense[0]) ) )
            //cudaDeviceSynchronize();
            CHECK_CUSPARSE( cusparseDcsr2dense( cuspHandle, BATCH_SIZE, width, descrC, \
                                                    cValDev, cRowDev, cColDev, \
                                                    cDense, BATCH_SIZE ) )
            cudaDeviceSynchronize();
            
            // Call activation kernel
            //  - Kernel adds bias and applies activation.
            //  - Updates dense matrix in place

            biasActivation<<<numBlocks,numThreads>>>( cDense, biasVectorDev, BATCH_SIZE, width );
            cudaDeviceSynchronize();

            //  - Compute NNZ per row
            CHECK_CUDA( cudaMalloc( (void**) &nnzPerRow, BATCH_SIZE*sizeof(int) ) )
            //cudaDeviceSynchronize();
            CHECK_CUSPARSE( cusparseDnnz( cuspHandle, CUSPARSE_DIRECTION_ROW, BATCH_SIZE, width, genericDescr, \
                                            cDense, BATCH_SIZE, nnzPerRow, nnzYPtr ) )
            //cudaDeviceSynchronize();

            //  - Re-allocate memory to y
            cudaFree(yRowDev);
            cudaFree(yColDev);
            cudaFree(yValDev);
            CHECK_CUDA( cudaMalloc( (void**) &yRowDev, (BATCH_SIZE+1)*sizeof(yRowDev[0]) ) )
            CHECK_CUDA( cudaMalloc( (void**) &yColDev, nnzY          *sizeof(yColDev[0]) ) )
            CHECK_CUDA( cudaMalloc( (void**) &yValDev, nnzY          *sizeof(yValDev[0]) ) )
            //cudaDeviceSynchronize();

            //  - Convert to sparse matrix
            CHECK_CUSPARSE( cusparseDdense2csr( cuspHandle, BATCH_SIZE, width, genericDescr, \
                                                    cDense, BATCH_SIZE, nnzPerRow, \
                                                    yValDev, yRowDev, yColDev ) )
            cudaDeviceSynchronize();

            // Loop cleanup
            cudaFree(buffer);
            cudaFree(cDense);
            cudaFree(cRowDev);
            cudaFree(cColDev);
            cudaFree(cValDev);
            cudaFree(nnzPerRow);
            //cudaDeviceSynchronize();
        }
        
        // Append batch results to vector
        int * yOutDev = 0;
        CHECK_CUDA( cudaMalloc( (void**) &yOutDev, nnzY*sizeof(labelOutDev[0]) ) )
        CHECK_CUSPARSE( cusparseXcsr2coo( cuspHandle, yRowDev, nnzY, BATCH_SIZE, yOutDev, CUSPARSE_INDEX_BASE_ZERO ) )
        cudaDeviceSynchronize();

        subOffset<<<numBlocks,numThreads>>>( yOutDev, 0-batch*BATCH_SIZE, nnzY );
        cudaDeviceSynchronize();

        labelOutDev.insert(labelOutDev.end(), yOutDev, yOutDev + nnzY);

        cudaFree(yOutDev);
        cudaFree(yRowDev);
        cudaFree(yColDev);
        cudaFree(yValDev);
    }

    // Destroy library something
    cusparseDestroyCsrgemm2Info(info);
    //cudaDeviceSynchronize();

    // Copy unique results to host.
    thrust::device_vector<int>::iterator it = thrust::unique(labelOutDev.begin(), labelOutDev.end());
    
    int outSize = thrust::distance(labelOutDev.begin(), it);
    labelOutHost.resize(outSize);
    thrust::copy(labelOutDev.begin(), it, labelOutHost.begin());            

    return outSize;
}

/******************
** MAIN FUNCTION **
******************/

int main(int argc, char **argv) {
    // Read arguments and update global parameters
    for ( int i = 1; i < argc; i++ ) {
        if (std::string(argv[i]) == "-n") {
            i++;
            std::stringstream ss(argv[i]);
            NEURONS.clear();
            while (ss.good()) {
                std::string num;
                std::getline(ss, num, ',');
                NEURONS.push_back(stoi(num));
            }
        }
        else if (std::string(argv[i]) == "-l") {
            i++;
            std::stringstream ss(argv[i]);
            LAYERS.clear();
            while(ss.good()) {
                std::string num;
                std::getline(ss, num, ',');
                LAYERS.push_back(stoi(num));
            }
        }
        else if (std::string(argv[i]) == "-nb") {
            i++;
            std::string num (argv[i]);
            NUM_BATCH = stoi(num);
            BATCH_SIZE = INPUT_SIZE/NUM_BATCH;
        }
        else {
            std::cout<<"Invalid argument: "<<argv[i]<<";\n";
            return 1;
        }
    }

    int idx = 0;
    // For each input width:
    for (int width : NEURONS) {
        std::cout<<"WIDTH: "<<width<<std::endl;
        // Load image data
        int imgDataSize = loadImageData(width);
        //cudaDeviceSynchronize();

        // Load Bias Vector
        biasVectorHost = (double *) malloc(width*sizeof(biasVectorHost[0]));
        std::fill_n(biasVectorHost, width, BIAS[idx]);

        // For each network depth:
        for (int depth : LAYERS) {
            // Load labels
            std::cout<<"\tDepth: "<<depth<<std::endl;
            int lblDataSize = 0;
            if ( depth == 120 || depth == 480 || depth == 1920 ) {
                lblDataSize = loadLabels(width, depth);
                //cudaDeviceSynchronize();
            }

            
            // For each layer
            weightDataRowHost = (int **)    malloc(depth*sizeof(weightDataRowHost[0]));
            weightDataColHost = (int **)    malloc(depth*sizeof(weightDataColHost[0]));
            weightDataValHost = (double **) malloc(depth*sizeof(weightDataValHost[0]));
            weightDataNNZHost = (int *)     malloc(depth*sizeof(weightDataNNZHost[0]));

            for (int layer = 0; layer < depth; layer++) {
                // Load weight matrices
                weightDataNNZHost[layer] = loadWeight(width, layer);
                //cudaDeviceSynchronize();
            }
            std::cout<<"\t\tWeights Loaded\n";
            
            // Initialise GPU:
            //      - Allocate memory on device.
            //      - Copy data from Host to device.
            //      - Initialise bias vector
            //      - Initialise CuSPARSE library
            //      - Format conversion 
            //      - Initialise matrix descriptors
            //      - Initialise Device
            initGPU(width, depth, imgDataSize, weightDataNNZHost);
            cudaDeviceSynchronize();
            std::cout<<"\t\tINIT COMPLETE\n";

            // Run the inference loop 
            auto beg = std::chrono::high_resolution_clock::now();

            int lblOutSize = runInference(width, depth, weightDataNNZHost);

            auto end = std::chrono::high_resolution_clock::now();
            auto tt = std::chrono::duration_cast<std::chrono::milliseconds>(end-beg);
            std::cout<<"\t\tInference complete.\n";
            std::cout<<"\t\t\tTime taken: "<<tt.count()<<" ms.\n";
            double num_edge = (double(INPUT_SIZE) * depth * width * 32)/(1e+9); // Edges (in bns)
            std::cout<<"\t\t\tThroughput: "<<(num_edge*1000)/double(tt.count())<<" bn edges/sec.\n";

            //for (int i = 0; i < lblOutSize; i++) {
            //    if (i < lblDataSize) std::cout<<labelDataHost[i];
            //    std::cout<<"\t"<<labelOutHost[i]<<std::endl;
            //}

            // Check if output matches
            if ( depth == 120 || depth == 480 || depth == 1920 ) {
                //std::cout<<lblDataSize<<" "<<truLbl.size()<<" "<<infOut.size()<<" "<<lblOutSize<<" "<<(infOut == truLbl)<<std::endl;
                if ( thrust::equal(labelDataHost, labelDataHost + lblDataSize, labelOutHost.begin()) && (lblDataSize == lblOutSize) ) std::cout<<"\t\tOutput matches.\n";
                else {
                    std::cout<<"\t\tERROR: Inference output mismatch.\tData Size: "<<lblDataSize<<"; Output size: "<<lblOutSize<<std::endl;
                    return 1;
                }
            }

            // Clean up
            if ( depth == 120 || depth == 480 || depth == 1920 ) free(labelDataHost);
            for ( int layer = 0; layer < depth; layer++ ) {
                cudaFree(weightDataRowHost[layer]);
                cudaFree(weightDataColHost[layer]);
                cudaFree(weightDataValHost[layer]);
            }
            free(weightDataRowHost);
            free(weightDataColHost);
            free(weightDataValHost);
            free(weightDataNNZHost);
            labelOutHost.clear();
            labelOutHost.shrink_to_fit();
            freeGPU(depth);
            std::cout<<"\t\tCleanup complete\n";
        }

        idx++; 

        // Clean up
        free(imageDataRowHost);
        free(imageDataColHost);
        free(imageDataValHost);
        free(biasVectorHost);
    }

    return 0;
}
