#!/bin/bash 
#PBS -k oe 
#PBS -l nodes=1:ppn=1,gpus=1,walltime=24:00:00
#PBS -M vdodeja2@illinois.edu
#PBS -m ae 
#PBS -N 533CR
#PBS -j oe 
cd ./projects.link/CS533/code/sparse-dnn-inference/
rm exe/$2
nvcc code/$1 -std=c++11 -lcusparse -o exe/$2 |& tee results/compile_results.txt
./exe/$2 ${@:4} |& tee results/$3-results.txt
